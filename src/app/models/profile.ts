export class Profile {
    id?: number;
    name?: string;
}

export class ProfileMapper {
    static toProfile(profile: any): Profile {
        if (profile) {
            return {
                id: profile.id,
                name: profile.nome
            }
        }
        return null;
    }

    static fromProfile(profile: Profile): any {
        if (profile) {
            return {
                id: profile.id,
                nome: profile.name
            }
        }
        return null;
    }
}
