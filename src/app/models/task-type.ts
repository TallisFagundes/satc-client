export class TaskType {
    id: number;
    name: string;
    usesCIS: boolean;
}

export class TaskTypeMapper {
    static toTaskType(taskType: any): TaskType {
        if (taskType) {
            return {
                id: taskType.id,
                name: taskType.nome,
                usesCIS: taskType.utilizaCis
            }
        }
        return null;
    }

    static fromTaskType(taskType: TaskType): any {
        if (taskType) {
            return {
                id: taskType.id,
                nome: taskType.name,
                utilizaCis: taskType.usesCIS
            }
        }
        return null;
    }
}