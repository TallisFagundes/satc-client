import { Area, AreaMapper } from './area';

export class Occupation {
    id?: number;
    name?: string;
    area?: Area;
    numeration?: string;
}

export class OccupationMapper {
    static toOccupation(occupation: any): Occupation {
        if (occupation) {
            return {
                id: occupation.id,
                name: occupation.nome,
                area: AreaMapper.toArea(occupation.area),
                numeration: occupation.numeracao
            }
        }
        return null;
    }

    static fromOccupation(occupation: Occupation): any {
        if (occupation) {
            return {
                id: occupation.id,
                nome: occupation.name,
                idArea: occupation.area.id,
                numeracao: occupation.numeration,
                indices: []
            }
        }
        return null;
    }
}
