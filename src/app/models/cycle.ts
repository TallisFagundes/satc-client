export class Cycle {
    id: number;
    name: string;
    startDate: string;
    endDate: string;
}

export class CycleMapper {
    static toCycle(cycle: any): Cycle {
        if (cycle) {
            return {
                id: cycle.id,
                name: cycle.descricao,
                startDate: cycle.inicio,
                endDate: cycle.fim
            }
        }
        return null;
    }

    static fromCycle(cycle: Cycle): any {
        if (cycle) {
            return {
                id: cycle.id,
                descricao: cycle.name,
                inicio: cycle.startDate,
                fim: cycle.endDate
            }
        }
        return null;
    }
}