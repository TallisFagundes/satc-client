import { Profile, ProfileMapper } from './profile';
import { Occupation, OccupationMapper } from './occupation';

export class Contributor {
    id?: number;
    cpf?: string;
    email?: string;
    name?: string;
    occupations?: Occupation[];
    profile?: Profile;
    rg?: string;
    password?: string;
    phone?: string;
}

export class ContributorMapper {
    static toContributor(contributor: any): Contributor {
        if (contributor) {
            return {
                id: contributor.id,
                name: contributor.nome,
                email: contributor.email,
                cpf: contributor.cpf,
                rg: contributor.rg,
                password: contributor.senha,
                phone: contributor.telefone,
                occupations: ContributorMapper.parseToOccupations(contributor.ocupacoes),
                profile: ProfileMapper.toProfile(contributor.perfil)
            }
        }
        return null;
    }

    static parseToOccupations(occupations: any[]): Occupation[] {
        const parsedOccupations: Occupation[] = [];
        occupations.forEach(
            (occupation) => {
                parsedOccupations.push(OccupationMapper.toOccupation(occupation));
            }
        );
        return parsedOccupations;
    }

    static fromContributor(contributor: Contributor): any {
        if (contributor) {
            return {
                id: contributor.id,
                nome: contributor.name,
                email: contributor.email,
                cpf: contributor.cpf,
                rg: contributor.rg,
                senha: contributor.password,
                telefone: contributor.phone,
                ocupacoes: ContributorMapper.parseFromOccupations(contributor.occupations),
                idPerfil: ProfileMapper.fromProfile(contributor.profile).id
            };
        }
        return null;
    }

    static parseFromOccupations(occupations: Occupation[]): any[] {
        const parsedOccupations: any[] = [];
        occupations.forEach(
            (occupation) => {
                parsedOccupations.push(OccupationMapper.fromOccupation(occupation).id);
            }
        );
        return parsedOccupations;
    }
}
