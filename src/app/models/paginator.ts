import { MatPaginatorIntl } from '@angular/material';

export class MatPaginatorIntlCro extends MatPaginatorIntl {
  lastPageLabel = 'Última';
  nextPageLabel = 'Próxima';
  previousPageLabel = 'Anterior';
  itemsPerPageLabel = 'Itens por página';
}
