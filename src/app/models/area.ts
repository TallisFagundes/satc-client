export class Area {
    id?: number;
    name?: string;
}

export class AreaMapper {
    static toArea(area: any): Area {
        if (area) {
            return {
                id: area.id,
                name: area.nome
            }
        }
        return null;
    }

    static fromArea(area: Area): any {
        if (area) {
            return {
                id: area.id,
                nome: area.name,
                cor: '0'
            }
        }
        return null;
    }
}
