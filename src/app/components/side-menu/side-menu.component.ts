import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { UserService } from '../../services/user.service';
import { LocalUserData } from '../../models/local-user-data';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements OnInit, OnDestroy {
  @Output() routeChanged: EventEmitter<void> = new EventEmitter<void>();

  userData: LocalUserData;
  routeChangeSubscription: any;

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.userData = this.userService.getUserLocalData();

    this.routeChangeSubscription = this.router.events.pipe(
      filter((event) => event instanceof NavigationEnd)
    ).subscribe(
      () => {
        this.routeChanged.emit();
      }
    );
  }

  ngOnDestroy() {
    this.routeChangeSubscription.unsubscribe();
  }

}
