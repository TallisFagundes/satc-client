import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginData: {
    email: string;
    password: string;
  };

  constructor(private authService: AuthService, private router: Router, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.loginData = {
      email: '',
      password: ''
    };
  }

  submitted(form: NgForm) {
    if (form.valid) {
      this.authService.login(form.value.email, form.value.password).subscribe(
        () => {
          this.router.navigateByUrl('home');
        },
        (error) => {
          this.snackBar.open(error.message, 'Erro', {
            duration: 2000,
          });
        }
      );
    }
  }

}
