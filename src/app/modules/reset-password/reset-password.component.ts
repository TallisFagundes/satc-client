import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  token: string;
  submittedSuccessfully: boolean = false;
  passwordDontMatch: boolean;
  passwordData: {
    password: string,
    confirmPassword: string
  };

  constructor(private activatedRoute: ActivatedRoute, private usersService: UserService) { }

  ngOnInit() {
    this.passwordData = {
      password: '',
      confirmPassword: ''
    };
    this.activatedRoute.queryParamMap.subscribe(
      (values: any) => {
        this.token = values.params.token;
        this.getPasswordTokenInfo();
      }
    );
  }

  getPasswordTokenInfo() {
    console.log(this.usersService.getPasswordTokenInfo(this.token));
  }

  setNewPassword(form: NgForm) {
    this.checkPassword(form);
    if (form.valid) {
      console.log('chegou');
      this.usersService.setNewPasswordByToken(this.token, form.value).subscribe(
        () => {
          this.submittedSuccessfully = true;
        }
      );
    }
  }

  checkPassword(form: NgForm) {
    if (form.controls.confirmPassword.touched || form.controls.confirmPassword.dirty) {
      if (form.controls.confirmPassword.value !== form.controls.password.value) {
        form.controls.confirmPassword.setErrors({ match: true })
      } else {
        form.controls.confirmPassword.updateValueAndValidity();
      }
    }
  }

}
