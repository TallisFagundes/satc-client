import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ProfileFormModalComponent } from '../profile-form-modal/profile-form-modal.component';
import { ProfilesService } from 'src/app/services/profiles.service';
import { Profile } from 'src/app/models/profile';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { ProfileDeleteModalComponent } from '../profile-delete-modal/profile-delete-modal.component';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-profiles',
  templateUrl: './profiles.component.html',
  styleUrls: ['./profiles.component.scss']
})
export class ProfilesComponent implements OnInit {
  profiles: Profile[];
  displayedColumns: string[];
  totalItems: number;
  nameSearchedForm: FormControl;
  isLoading: boolean;

  constructor(
    private profilesService: ProfilesService,
    private materialDialog: MatDialog
  ) { }

  ngOnInit() {
    this.displayedColumns = ['name', 'actions'];
    this.nameSearchedForm = new FormControl();
    this.nameSearchedForm.valueChanges.pipe(debounceTime(500)).subscribe(
      (newValue) => {
        this.getProfiles({ search: newValue });
      }
    );
    this.getProfiles();
  }

  getProfiles(params?: { search?: string, page?: number }): void {
    this.isLoading = true;
    this.profilesService.getProfiles(params)
      .pipe(finalize(() => {
        this.isLoading = false;
      }))
      .subscribe(
        (response: { items: Profile[], totalItems: number }) => {
          this.profiles = response.items;
          this.totalItems = response.totalItems;
        }
      );
  }

  changedPage(page) {
    this.getProfiles({ search: this.nameSearchedForm.value, page: page.pageIndex });
  }

  showProfileModal(profile?: Profile) {
    const dialogRef = this.materialDialog.open(ProfileFormModalComponent,
      {
        maxWidth: '400px',
        width: '400px',
        maxHeight: '90vh',
        data: {
          profile: profile ? Object.assign({}, profile) : undefined
        }
      });
    dialogRef.afterClosed().subscribe(
      (response) => {
        if (response && response.success) {
          this.getProfiles();
        }
      }
    );
  }

  deleteProfile(profile: Profile) {
    const dialogRef = this.materialDialog.open(ProfileDeleteModalComponent, {
      data: {
        profile: profile
      }
    });
    dialogRef.afterClosed().subscribe(
      (response: { removed: boolean }) => {
        if (response && response.removed) {
          this.getProfiles();
        }
      }
    );
  }

}
