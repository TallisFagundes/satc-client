import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfilesComponent } from './profiles-list/profiles.component';
import { ProfileFormModalComponent } from './profile-form-modal/profile-form-modal.component';
import { ProfileDeleteModalComponent } from './profile-delete-modal/profile-delete-modal.component';

const routes: Routes = [
  {
    path: '',
    component: ProfilesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfilesRoutingModule { }

export const routedComponents = [ProfilesComponent, ProfileFormModalComponent, ProfileDeleteModalComponent];
