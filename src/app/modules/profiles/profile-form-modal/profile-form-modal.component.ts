import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Profile } from 'src/app/models/profile';
import { NgForm } from '@angular/forms';
import { Occupation } from 'src/app/models/occupation';
import { ProfilesService } from 'src/app/services/profiles.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-profile-form-modal',
  templateUrl: './profile-form-modal.component.html',
  styleUrls: ['./profile-form-modal.component.scss']
})
export class ProfileFormModalComponent implements OnInit {
  modalTitle: string;
  isLoading: boolean;

  constructor(
    private dialogRef: MatDialogRef<ProfileFormModalComponent>,
    @Inject(MAT_DIALOG_DATA) public modalData: ModalData,
    private profilesService: ProfilesService
  ) { }

  ngOnInit() {
    if (this.modalData && this.modalData.profile && this.modalData.profile.id) {
      this.modalTitle = 'Editar Perfil';
    } else {
      this.modalTitle = 'Novo Perfil';
      this.modalData.profile = new Profile();
    }
  }

  save(form: NgForm) {
    if (form.valid) {
      this.isLoading = true;
      form.form.disable();
      form.value.id = this.modalData.profile.id;
      if (form.value.id) {
        this.profilesService.updateProfile(form.value)
          .pipe(finalize(() => {
            form.form.enable();
            this.isLoading = false;
          }))
          .subscribe(
            () => {
              this.isLoading = false;
              this.dialogRef.close({ success: true });
            }
          );
      } else {
        this.profilesService.createProfile(form.value)
          .pipe(finalize(() => {
            form.form.enable();
            this.isLoading = false;
          }))
          .subscribe(
            () => {
              this.isLoading = false;
              this.dialogRef.close({ success: true });
            }
          );
      }
    }
  }

  cancel() {
    this.dialogRef.close();
  }

}

export class ModalData {
  profile: Profile;
  occupations: Occupation[];
  profiles: Profile[];
}
