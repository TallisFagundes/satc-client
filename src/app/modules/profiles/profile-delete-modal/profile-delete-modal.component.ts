import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Profile } from 'src/app/models/profile';
import { ProfilesService } from 'src/app/services/profiles.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-profile-delete-modal',
  templateUrl: './profile-delete-modal.component.html',
  styleUrls: ['./profile-delete-modal.component.scss']
})
export class ProfileDeleteModalComponent implements OnInit {
  isLoading: boolean;

  constructor(
    private dialogRef: MatDialogRef<ProfileDeleteModalComponent>,
    @Inject(MAT_DIALOG_DATA) public modalData: ModalData,
    private profilesService: ProfilesService
  ) { }

  ngOnInit() {
  }

  confirm() {
    this.isLoading = true;
    this.profilesService.deleteProfile(this.modalData.profile.id)
      .pipe(finalize(() => this.isLoading = false))
      .subscribe(
        (deleted: boolean) => {
          if (deleted) {
            this.dialogRef.close({ removed: true });
          }
        }
      );
  }

  cancel() {
    this.dialogRef.close({ removed: false });
  }

}

export class ModalData {
  profile: Profile;
}
