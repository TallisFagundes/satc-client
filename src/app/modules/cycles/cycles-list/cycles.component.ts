import { Component, OnInit } from '@angular/core';
import { Cycle } from '../../../models/cycle';
import { CyclesService } from '../../../services/cycles.service';
import { MatDialog } from '@angular/material';
import { CycleFormModalComponent } from '../cycle-form-modal/cycle-form-modal.component';
import { OccupationsService } from 'src/app/services/occupations.service';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { CycleDeleteModalComponent } from '../cycle-delete-modal/cycle-delete-modal.component';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-cycles',
  templateUrl: './cycles.component.html',
  styleUrls: ['./cycles.component.scss']
})
export class CyclesComponent implements OnInit {
  cycles: Cycle[];
  displayedColumns: string[];
  totalItems: number;
  nameSearchedForm: FormControl;
  isLoading: boolean;

  constructor(
    private cyclesService: CyclesService,
    private materialDialog: MatDialog
  ) { }

  ngOnInit() {
    this.displayedColumns = ['name', 'startDate', 'endDate', 'actions'];
    this.nameSearchedForm = new FormControl();
    this.nameSearchedForm.valueChanges.pipe(debounceTime(500)).subscribe(
      (newValue) => {
        this.getCycles({ search: newValue });
      }
    );
    this.getCycles();
  }

  getCycles(params?: { search?: string, page?: number }): void {
    this.isLoading = true;
    this.cyclesService.getCycles(params)
      .pipe(finalize(() => {
        this.isLoading = false;
      }))
      .subscribe(
        (response: { items: Cycle[], totalItems: number }) => {
          this.cycles = response.items;
          this.totalItems = response.totalItems;
        }
      );
  }

  changedPage(page) {
    this.getCycles({ search: this.nameSearchedForm.value, page: page.pageIndex });
  }

  showCycleModal(cycle?: Cycle) {
    const dialogRef = this.materialDialog.open(CycleFormModalComponent,
      {
        maxWidth: '400px',
        width: '400px',
        maxHeight: '90vh',
        data: {
          cycle: cycle ? Object.assign({}, cycle) : undefined
        }
      });
    dialogRef.afterClosed().subscribe(
      (response) => {
        if (response && response.success) {
          this.getCycles();
        }
      }
    );
  }

  deleteCycle(cycle: Cycle) {
    const dialogRef = this.materialDialog.open(CycleDeleteModalComponent, {
      data: {
        cycle: cycle
      }
    });
    dialogRef.afterClosed().subscribe(
      (response: { removed: boolean }) => {
        if (response && response.removed) {
          this.getCycles();
        }
      }
    );
  }

}
