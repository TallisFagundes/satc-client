import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CycleDeleteModalComponent } from './cycle-delete-modal.component';

describe('CycleDeleteModalComponent', () => {
  let component: CycleDeleteModalComponent;
  let fixture: ComponentFixture<CycleDeleteModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CycleDeleteModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CycleDeleteModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
