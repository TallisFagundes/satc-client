import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Cycle } from 'src/app/models/cycle';
import { CyclesService } from 'src/app/services/cycles.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-cycle-delete-modal',
  templateUrl: './cycle-delete-modal.component.html',
  styleUrls: ['./cycle-delete-modal.component.scss']
})
export class CycleDeleteModalComponent implements OnInit {
  isLoading: boolean;

  constructor(
    private dialogRef: MatDialogRef<CycleDeleteModalComponent>,
    @Inject(MAT_DIALOG_DATA) public modalData: ModalData,
    private cyclesService: CyclesService
  ) { }

  ngOnInit() {
  }

  confirm() {
    this.isLoading = true;
    this.cyclesService.deleteCycle(this.modalData.cycle.id)
      .pipe(finalize(() => this.isLoading = false))
      .subscribe(
        (deleted: boolean) => {
          if (deleted) {
            this.dialogRef.close({ removed: true });
          }
        }
      );
  }

  cancel() {
    this.dialogRef.close({ removed: false });
  }

}

export class ModalData {
  cycle: Cycle;
}
