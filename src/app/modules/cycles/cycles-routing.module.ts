import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CyclesComponent } from './cycles-list/cycles.component';
import { CycleFormModalComponent } from './cycle-form-modal/cycle-form-modal.component';
import { CycleDeleteModalComponent } from './cycle-delete-modal/cycle-delete-modal.component';

const routes: Routes = [
  {
    path: '',
    component: CyclesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CyclesRoutingModule { }

export const routedComponents = [CyclesComponent, CycleFormModalComponent, CycleDeleteModalComponent];
