import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgSelectModule } from '@ng-select/ng-select';
import {
  MatTableModule,
  MatInputModule,
  MatIconModule,
  MatButtonModule,
  MatPaginatorModule,
  MatPaginatorIntl,
  MatDialogModule,
  MatMenuModule,
  MatProgressSpinnerModule,
  MatProgressBarModule,
  MatDatepickerModule
} from '@angular/material';

import { CyclesRoutingModule, routedComponents } from './cycles-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatPaginatorIntlCro } from 'src/app/models/paginator';
import { CycleFormModalComponent } from './cycle-form-modal/cycle-form-modal.component';
import { NgxMaskModule } from 'ngx-mask';
import { CycleDeleteModalComponent } from './cycle-delete-modal/cycle-delete-modal.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatButtonModule,
    CyclesRoutingModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatMenuModule,
    MatProgressBarModule,
    MatDatepickerModule,
    NgSelectModule,
    NgxMaskModule
  ],
  entryComponents: [CycleFormModalComponent, CycleDeleteModalComponent],
  providers: [{ provide: MatPaginatorIntl, useClass: MatPaginatorIntlCro }],
  declarations: [routedComponents]
})
export class CyclesModule { }
