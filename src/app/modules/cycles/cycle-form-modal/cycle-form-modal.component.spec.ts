import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CycleFormModalComponent } from './cycle-form-modal.component';

describe('CycleFormModalComponent', () => {
  let component: CycleFormModalComponent;
  let fixture: ComponentFixture<CycleFormModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CycleFormModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CycleFormModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
