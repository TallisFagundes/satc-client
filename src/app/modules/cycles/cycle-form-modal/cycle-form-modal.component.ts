import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Cycle } from 'src/app/models/cycle';
import { NgForm, FormControl } from '@angular/forms';
import { CyclesService } from 'src/app/services/cycles.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-cycle-form-modal',
  templateUrl: './cycle-form-modal.component.html',
  styleUrls: ['./cycle-form-modal.component.scss']
})
export class CycleFormModalComponent implements OnInit {
  modalTitle: string;
  isLoading: boolean;

  constructor(
    private dialogRef: MatDialogRef<CycleFormModalComponent>,
    @Inject(MAT_DIALOG_DATA) public modalData: ModalData,
    private cyclesService: CyclesService
  ) { }

  ngOnInit() {
    if (this.modalData && this.modalData.cycle && this.modalData.cycle.id) {
      this.modalTitle = 'Editar Ciclo';
      this.modalData.cycle.startDate = this.formatDate(this.modalData.cycle.startDate);
      this.modalData.cycle.endDate = this.formatDate(this.modalData.cycle.endDate);
    } else {
      this.modalTitle = 'Novo Ciclo';
      this.modalData.cycle = new Cycle();
    }
  }

  formatDate(date) {
    const d = new Date(date);
    const year = d.getFullYear();
    let month = '' + (d.getMonth() + 1);
    let day = '' + d.getDate();

    if (month.length < 2) {
      month = '0' + month;
    }
    if (day.length < 2) {
      day = '0' + day;
    }

    return [year, month, day].join('-');
  }

  save(form: NgForm) {
    if (form.valid) {
      this.isLoading = true;
      form.form.disable();
      form.value.id = this.modalData.cycle.id;
      if (form.value.id) {
        this.cyclesService.updateCycle(form.value)
          .pipe(finalize(() => {
            form.form.enable();
            this.isLoading = false;
          }))
          .subscribe(
            () => {
              this.isLoading = false;
              this.dialogRef.close({ success: true });
            }
          );
      } else {
        this.cyclesService.createCycle(form.value)
          .pipe(finalize(() => {
            form.form.enable();
            this.isLoading = false;
          }))
          .subscribe(
            () => {
              this.isLoading = false;
              this.dialogRef.close({ success: true });
            }
          );
      }
    }
  }

  cancel() {
    this.dialogRef.close();
  }

}

export class ModalData {
  cycle: Cycle;
}
