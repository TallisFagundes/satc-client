import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OccupationDeleteModalComponent } from './occupation-delete-modal.component';

describe('OccupationDeleteModalComponent', () => {
  let component: OccupationDeleteModalComponent;
  let fixture: ComponentFixture<OccupationDeleteModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OccupationDeleteModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OccupationDeleteModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
