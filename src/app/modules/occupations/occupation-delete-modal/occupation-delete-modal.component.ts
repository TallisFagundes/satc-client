import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Occupation } from 'src/app/models/occupation';
import { OccupationsService } from 'src/app/services/occupations.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-occupation-delete-modal',
  templateUrl: './occupation-delete-modal.component.html',
  styleUrls: ['./occupation-delete-modal.component.scss']
})
export class OccupationDeleteModalComponent implements OnInit {
  isLoading: boolean;

  constructor(
    private dialogRef: MatDialogRef<OccupationDeleteModalComponent>,
    @Inject(MAT_DIALOG_DATA) public modalData: ModalData,
    private occupationsService: OccupationsService
  ) { }

  ngOnInit() {
  }

  confirm() {
    this.isLoading = true;
    this.occupationsService.deleteOccupation(this.modalData.occupation.id)
      .pipe(finalize(() => this.isLoading = false))
      .subscribe(
        (deleted) => {
          if (deleted) {
            this.dialogRef.close({ removed: true });
          }
        }
      );
  }

  cancel() {
    this.dialogRef.close({ removed: false });
  }

}

export class ModalData {
  occupation: Occupation;
}
