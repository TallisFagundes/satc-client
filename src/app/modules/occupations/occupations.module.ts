import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgSelectModule } from '@ng-select/ng-select';
import {
  MatTableModule,
  MatInputModule,
  MatIconModule,
  MatButtonModule,
  MatPaginatorModule,
  MatPaginatorIntl,
  MatDialogModule,
  MatMenuModule,
  MatProgressSpinnerModule,
  MatProgressBarModule
} from '@angular/material';

import { OccupationsRoutingModule, routedComponents } from './occupations-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatPaginatorIntlCro } from 'src/app/models/paginator';
import { OccupationFormModalComponent } from './occupation-form-modal/occupation-form-modal.component';
import { NgxMaskModule } from 'ngx-mask';
import { OccupationDeleteModalComponent } from './occupation-delete-modal/occupation-delete-modal.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatButtonModule,
    OccupationsRoutingModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatMenuModule,
    MatProgressBarModule,
    NgSelectModule,
    NgxMaskModule
  ],
  entryComponents: [OccupationFormModalComponent, OccupationDeleteModalComponent],
  providers: [{ provide: MatPaginatorIntl, useClass: MatPaginatorIntlCro }],
  declarations: [routedComponents]
})
export class OccupationsModule { }
