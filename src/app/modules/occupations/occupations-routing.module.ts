import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OccupationsComponent } from './occupations-list/occupations.component';
import { OccupationFormModalComponent } from './occupation-form-modal/occupation-form-modal.component';
import { OccupationDeleteModalComponent } from './occupation-delete-modal/occupation-delete-modal.component';

const routes: Routes = [
  {
    path: '',
    component: OccupationsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OccupationsRoutingModule { }

export const routedComponents = [OccupationsComponent, OccupationFormModalComponent, OccupationDeleteModalComponent];
