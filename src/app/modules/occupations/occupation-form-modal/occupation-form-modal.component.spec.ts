import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OccupationFormModalComponent } from './occupation-form-modal.component';

describe('OccupationFormModalComponent', () => {
  let component: OccupationFormModalComponent;
  let fixture: ComponentFixture<OccupationFormModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OccupationFormModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OccupationFormModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
