import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Occupation } from 'src/app/models/occupation';
import { NgForm } from '@angular/forms';
import { OccupationsService } from 'src/app/services/occupations.service';
import { finalize } from 'rxjs/operators';
import { Area } from 'src/app/models/area';

@Component({
  selector: 'app-occupation-form-modal',
  templateUrl: './occupation-form-modal.component.html',
  styleUrls: ['./occupation-form-modal.component.scss']
})
export class OccupationFormModalComponent implements OnInit {
  modalTitle: string;
  isLoading: boolean;

  constructor(
    private dialogRef: MatDialogRef<OccupationFormModalComponent>,
    @Inject(MAT_DIALOG_DATA) public modalData: ModalData,
    private occupationsService: OccupationsService
  ) { }

  ngOnInit() {
    if (this.modalData && this.modalData.occupation && this.modalData.occupation.id) {
      this.modalTitle = 'Editar Ocupação';
    } else {
      this.modalTitle = 'Novo Ocupação';
      this.modalData.occupation = new Occupation();
    }
  }

  save(form: NgForm) {
    if (form.valid) {
      this.isLoading = true;
      form.form.disable();
      form.value.id = this.modalData.occupation.id;
      if (form.value.id) {
        this.occupationsService.updateOccupation(form.value)
          .pipe(finalize(() => {
            form.form.enable();
            this.isLoading = false;
          }))
          .subscribe(
            () => {
              this.isLoading = false;
              this.dialogRef.close({ success: true });
            }
          );
      } else {
        this.occupationsService.createOccupation(form.value)
          .pipe(finalize(() => {
            form.form.enable();
            this.isLoading = false;
          }))
          .subscribe(
            () => {
              this.isLoading = false;
              this.dialogRef.close({ success: true });
            }
          );
      }
    }
  }

  cancel() {
    this.dialogRef.close();
  }

}

export class ModalData {
  occupation: Occupation;
  areas: Area[];
}
