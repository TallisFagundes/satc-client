import { Component, OnInit } from '@angular/core';
import { Occupation } from '../../../models/occupation';
import { OccupationsService } from '../../../services/occupations.service';
import { MatDialog } from '@angular/material';
import { OccupationFormModalComponent } from '../occupation-form-modal/occupation-form-modal.component';
import { Observable, forkJoin } from 'rxjs';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { OccupationDeleteModalComponent } from '../occupation-delete-modal/occupation-delete-modal.component';
import { finalize } from 'rxjs/operators';
import { AreasService } from 'src/app/services/areas.service';
import { Area } from 'src/app/models/area';

@Component({
  selector: 'app-occupations',
  templateUrl: './occupations.component.html',
  styleUrls: ['./occupations.component.scss']
})
export class OccupationsComponent implements OnInit {
  occupations: Occupation[];
  displayedColumns: string[];
  areas: Area[];
  totalItems: number;
  nameSearchedForm: FormControl;
  isLoading: boolean;

  constructor(
    private occupationsService: OccupationsService,
    private areasService: AreasService,
    private materialDialog: MatDialog
  ) { }

  ngOnInit() {
    this.displayedColumns = ['name', 'area.name', 'actions'];
    this.nameSearchedForm = new FormControl();
    this.nameSearchedForm.valueChanges.pipe(debounceTime(500)).subscribe(
      (newValue) => {
        this.getOccupations({ search: newValue });
      }
    );
    this.getOccupations();
  }

  getOccupations(params?: { search?: string, page?: number }): void {
    this.isLoading = true;
    this.occupationsService.getOccupations(params)
      .pipe(finalize(() => {
        this.isLoading = false;
      }))
      .subscribe(
        (response: { items: Occupation[], totalItems: number }) => {
          this.occupations = response.items;
          this.totalItems = response.totalItems;
        }
      );
  }

  changedPage(page) {
    this.getOccupations({ search: this.nameSearchedForm.value, page: page.pageIndex });
  }

  loadModalData(): Observable<void> {
    return Observable.create(
      (observer) => {

      }
    );
  }

  showOccupationModal(occupation?: Occupation) {
    if (!this.areas) {
      this.areasService.getAreas({}).subscribe(
        (response) => {
          this.areas = response.items;
          this.showModal(occupation);
        }
      );
    } else {
      this.showModal(occupation);
    }
  }

  showModal(occupation?: Occupation) {
    const dialogRef = this.materialDialog.open(OccupationFormModalComponent,
      {
        maxWidth: '400px',
        width: '400px',
        maxHeight: '90vh',
        data: {
          areas: this.areas,
          occupation: occupation ? Object.assign({}, occupation) : undefined
        }
      });
    dialogRef.afterClosed().subscribe(
      (response) => {
        if (response && response.success) {
          this.getOccupations();
        }
      }
    );
  }

  deleteOccupation(occupation: Occupation) {
    const dialogRef = this.materialDialog.open(OccupationDeleteModalComponent, {
      data: {
        occupation: occupation
      }
    });
    dialogRef.afterClosed().subscribe(
      (response: { removed: boolean }) => {
        if (response && response.removed) {
          this.getOccupations();
        }
      }
    );
  }

}
