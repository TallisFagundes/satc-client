import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgSelectModule } from '@ng-select/ng-select';
import {
  MatTableModule,
  MatInputModule,
  MatIconModule,
  MatButtonModule,
  MatPaginatorModule,
  MatPaginatorIntl,
  MatDialogModule,
  MatMenuModule,
  MatProgressSpinnerModule,
  MatProgressBarModule,
  MatSlideToggleModule
} from '@angular/material';

import { TaskTypesRoutingModule, routedComponents } from './task-types-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatPaginatorIntlCro } from 'src/app/models/paginator';
import { TaskTypeFormModalComponent } from './task-type-form-modal/task-type-form-modal.component';
import { NgxMaskModule } from 'ngx-mask';
import { TaskTypeDeleteModalComponent } from './task-type-delete-modal/task-type-delete-modal.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatButtonModule,
    TaskTypesRoutingModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatMenuModule,
    MatProgressBarModule,
    MatSlideToggleModule,
    NgSelectModule,
    NgxMaskModule
  ],
  entryComponents: [TaskTypeFormModalComponent, TaskTypeDeleteModalComponent],
  providers: [{ provide: MatPaginatorIntl, useClass: MatPaginatorIntlCro }],
  declarations: [routedComponents]
})
export class TaskTypesModule { }
