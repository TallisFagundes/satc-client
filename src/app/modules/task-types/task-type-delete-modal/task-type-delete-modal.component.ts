import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TaskType } from 'src/app/models/task-type';
import { TaskTypesService } from 'src/app/services/task-types.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-task-type-delete-modal',
  templateUrl: './task-type-delete-modal.component.html',
  styleUrls: ['./task-type-delete-modal.component.scss']
})
export class TaskTypeDeleteModalComponent implements OnInit {
  isLoading: boolean;

  constructor(
    private dialogRef: MatDialogRef<TaskTypeDeleteModalComponent>,
    @Inject(MAT_DIALOG_DATA) public modalData: ModalData,
    private taskTypesService: TaskTypesService
  ) { }

  ngOnInit() {
  }

  confirm() {
    this.isLoading = true;
    this.taskTypesService.deleteTaskType(this.modalData.taskType.id)
      .pipe(finalize(() => this.isLoading = false))
      .subscribe(
        (deleted: boolean) => {
          if (deleted) {
            this.dialogRef.close({ removed: true });
          }
        }
      );
  }

  cancel() {
    this.dialogRef.close({ removed: false });
  }

}

export class ModalData {
  taskType: TaskType;
}
