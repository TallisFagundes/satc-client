import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskTypeDeleteModalComponent } from './taskType-delete-modal.component';

describe('TaskTypeDeleteModalComponent', () => {
  let component: TaskTypeDeleteModalComponent;
  let fixture: ComponentFixture<TaskTypeDeleteModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskTypeDeleteModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskTypeDeleteModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
