import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskTypeFormModalComponent } from './taskType-form-modal.component';

describe('TaskTypeFormModalComponent', () => {
  let component: TaskTypeFormModalComponent;
  let fixture: ComponentFixture<TaskTypeFormModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskTypeFormModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskTypeFormModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
