import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TaskType } from 'src/app/models/task-type';
import { NgForm } from '@angular/forms';
import { Occupation } from 'src/app/models/occupation';
import { Profile } from 'src/app/models/profile';
import { TaskTypesService } from 'src/app/services/task-types.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-task-type-form-modal',
  templateUrl: './task-type-form-modal.component.html',
  styleUrls: ['./task-type-form-modal.component.scss']
})
export class TaskTypeFormModalComponent implements OnInit {
  modalTitle: string;
  isLoading: boolean;

  constructor(
    private dialogRef: MatDialogRef<TaskTypeFormModalComponent>,
    @Inject(MAT_DIALOG_DATA) public modalData: ModalData,
    private taskTypesService: TaskTypesService
  ) { }

  ngOnInit() {
    if (this.modalData && this.modalData.taskType && this.modalData.taskType.id) {
      this.modalTitle = 'Editar Tipo de tarefa';
    } else {
      this.modalTitle = 'Novo Tipo de tarefa';
      this.modalData.taskType = new TaskType();
    }
  }

  save(form: NgForm) {
    if (form.valid) {
      this.isLoading = true;
      form.form.disable();
      form.value.id = this.modalData.taskType.id;
      if (form.value.id) {
        this.taskTypesService.updateTaskType(form.value)
          .pipe(finalize(() => {
            form.form.enable();
            this.isLoading = false;
          }))
          .subscribe(
            () => {
              this.isLoading = false;
              this.dialogRef.close({ success: true });
            }
          );
      } else {
        this.taskTypesService.createTaskType(form.value)
          .pipe(finalize(() => {
            form.form.enable();
            this.isLoading = false;
          }))
          .subscribe(
            () => {
              this.isLoading = false;
              this.dialogRef.close({ success: true });
            }
          );
      }
    }
  }

  cancel() {
    this.dialogRef.close();
  }

}

export class ModalData {
  taskType: TaskType;
  occupations: Occupation[];
  profiles: Profile[];
}
