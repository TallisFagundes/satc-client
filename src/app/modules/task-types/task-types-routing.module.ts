import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TaskTypesComponent } from './task-types-list/task-types.component';
import { TaskTypeFormModalComponent } from './task-type-form-modal/task-type-form-modal.component';
import { TaskTypeDeleteModalComponent } from './task-type-delete-modal/task-type-delete-modal.component';

const routes: Routes = [
  {
    path: '',
    component: TaskTypesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TaskTypesRoutingModule { }

export const routedComponents = [TaskTypesComponent, TaskTypeFormModalComponent, TaskTypeDeleteModalComponent];
