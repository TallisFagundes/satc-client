import { TaskTypesModule } from './task-types.module';

describe('TaskTypesModule', () => {
  let taskTypesModule: TaskTypesModule;

  beforeEach(() => {
    taskTypesModule = new TaskTypesModule();
  });

  it('should create an instance', () => {
    expect(taskTypesModule).toBeTruthy();
  });
});
