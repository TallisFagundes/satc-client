import { Component, OnInit } from '@angular/core';
import { TaskType } from '../../../models/task-type';
import { TaskTypesService } from '../../../services/task-types.service';
import { MatDialog } from '@angular/material';
import { TaskTypeFormModalComponent } from '../task-type-form-modal/task-type-form-modal.component';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { TaskTypeDeleteModalComponent } from '../task-type-delete-modal/task-type-delete-modal.component';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-task-types',
  templateUrl: './task-types.component.html',
  styleUrls: ['./task-types.component.scss']
})
export class TaskTypesComponent implements OnInit {
  taskTypes: TaskType[];
  displayedColumns: string[];
  totalItems: number;
  nameSearchedForm: FormControl;
  isLoading: boolean;

  constructor(
    private taskTypesService: TaskTypesService,
    private materialDialog: MatDialog
  ) { }

  ngOnInit() {
    this.displayedColumns = ['name', 'usesCIS', 'actions'];
    this.nameSearchedForm = new FormControl();
    this.nameSearchedForm.valueChanges.pipe(debounceTime(500)).subscribe(
      (newValue) => {
        this.getTaskTypes({ search: newValue });
      }
    );
    this.getTaskTypes();
  }

  getTaskTypes(params?: { search?: string, page?: number }): void {
    this.isLoading = true;
    this.taskTypesService.getTaskTypes(params)
      .pipe(finalize(() => {
        this.isLoading = false;
      }))
      .subscribe(
        (response: { items: TaskType[], totalItems: number }) => {
          this.taskTypes = response.items;
          this.totalItems = response.totalItems;
        }
      );
  }

  changedPage(page) {
    this.getTaskTypes({ search: this.nameSearchedForm.value, page: page.pageIndex });
  }

  showTaskTypeModal(taskType?: TaskType) {
    const dialogRef = this.materialDialog.open(TaskTypeFormModalComponent,
      {
        maxWidth: '400px',
        width: '400px',
        maxHeight: '90vh',
        data: {
          taskType: taskType ? Object.assign({}, taskType) : undefined
        }
      });
    dialogRef.afterClosed().subscribe(
      (response) => {
        if (response && response.success) {
          this.getTaskTypes();
        }
      }
    );
  }

  deleteTaskType(taskType: TaskType) {
    const dialogRef = this.materialDialog.open(TaskTypeDeleteModalComponent, {
      data: {
        taskType: taskType
      }
    });
    dialogRef.afterClosed().subscribe(
      (response: { removed: boolean }) => {
        if (response && response.removed) {
          this.getTaskTypes();
        }
      }
    );
  }

  translateUsesCIS(usesCIS: boolean): string {
    return usesCIS ? 'Sim' : 'Não';
  }

}
