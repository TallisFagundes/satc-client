import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DefaultLayoutComponent } from './default-layout.component';
import { SideMenuComponent } from '../../components/side-menu/side-menu.component';
import { ToolbarComponent } from '../../components/toolbar/toolbar.component';
import { MatSidenavModule, MatToolbarModule, MatButtonModule, MatIconModule } from '@angular/material';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    MatSidenavModule,
    RouterModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule
  ],
  declarations: [
    DefaultLayoutComponent,
    SideMenuComponent,
    ToolbarComponent
  ],
  exports: [
    DefaultLayoutComponent,
    SideMenuComponent,
    ToolbarComponent
  ]
})
export class DefaultLayoutModule { }
