import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExternalLayoutRoutingModule, routedComponents } from './external-layout-routing.module';
import { MatCardModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    ExternalLayoutRoutingModule,
    MatCardModule
  ],
  declarations: [routedComponents],
  exports: [routedComponents]
})
export class ExternalLayoutModule { }
