import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExternalLayoutComponent } from './external-layout.component';

const routes: Routes = [
  {
    path: '',
    component: ExternalLayoutComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExternalLayoutRoutingModule { }

export const routedComponents = [ExternalLayoutComponent];
