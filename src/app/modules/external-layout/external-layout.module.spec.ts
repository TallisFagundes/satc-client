import { ExternalLayoutModule } from './external-layout.module';

describe('ExternalLayoutModule', () => {
  let externalLayoutModule: ExternalLayoutModule;

  beforeEach(() => {
    externalLayoutModule = new ExternalLayoutModule();
  });

  it('should create an instance', () => {
    expect(externalLayoutModule).toBeTruthy();
  });
});
