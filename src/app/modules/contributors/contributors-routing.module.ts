import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContributorsComponent } from './contributors-list/contributors.component';
import { ContributorFormModalComponent } from './contributor-form-modal/contributor-form-modal.component';
import { ContributorDeleteModalComponent } from './contributor-delete-modal/contributor-delete-modal.component';

const routes: Routes = [
  {
    path: '',
    component: ContributorsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContributorsRoutingModule { }

export const routedComponents = [ContributorsComponent, ContributorFormModalComponent, ContributorDeleteModalComponent];
