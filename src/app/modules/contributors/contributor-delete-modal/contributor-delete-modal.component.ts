import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Contributor } from 'src/app/models/contributor';
import { ContributorsService } from 'src/app/services/contributors.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-contributor-delete-modal',
  templateUrl: './contributor-delete-modal.component.html',
  styleUrls: ['./contributor-delete-modal.component.scss']
})
export class ContributorDeleteModalComponent implements OnInit {
  isLoading: boolean;

  constructor(
    private dialogRef: MatDialogRef<ContributorDeleteModalComponent>,
    @Inject(MAT_DIALOG_DATA) public modalData: ModalData,
    private contributorsService: ContributorsService
  ) { }

  ngOnInit() {
  }

  confirm() {
    this.isLoading = true;
    this.contributorsService.deleteContributor(this.modalData.contributor.id)
      .pipe(finalize(() => this.isLoading = false))
      .subscribe(
        (deleted: boolean) => {
          if (deleted) {
            this.dialogRef.close({ removed: true });
          }
        }
      );
  }

  cancel() {
    this.dialogRef.close({ removed: false });
  }

}

export class ModalData {
  contributor: Contributor;
}
