import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContributorDeleteModalComponent } from './contributor-delete-modal.component';

describe('ContributorDeleteModalComponent', () => {
  let component: ContributorDeleteModalComponent;
  let fixture: ComponentFixture<ContributorDeleteModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContributorDeleteModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContributorDeleteModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
