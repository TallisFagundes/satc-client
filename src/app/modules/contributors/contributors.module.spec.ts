import { ContributorsModule } from './contributors.module';

describe('ContributorsModule', () => {
  let contributorsModule: ContributorsModule;

  beforeEach(() => {
    contributorsModule = new ContributorsModule();
  });

  it('should create an instance', () => {
    expect(contributorsModule).toBeTruthy();
  });
});
