import { Component, OnInit } from '@angular/core';
import { Contributor } from '../../../models/contributor';
import { ContributorsService } from '../../../services/contributors.service';
import { MatDialog } from '@angular/material';
import { ContributorFormModalComponent } from '../contributor-form-modal/contributor-form-modal.component';
import { ProfilesService } from 'src/app/services/profiles.service';
import { OccupationsService } from 'src/app/services/occupations.service';
import { Observable, forkJoin } from 'rxjs';
import { Profile } from 'src/app/models/profile';
import { Occupation } from 'src/app/models/occupation';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { ContributorDeleteModalComponent } from '../contributor-delete-modal/contributor-delete-modal.component';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-contributors',
  templateUrl: './contributors.component.html',
  styleUrls: ['./contributors.component.scss']
})
export class ContributorsComponent implements OnInit {
  contributors: Contributor[];
  displayedColumns: string[];
  profiles: Profile[];
  occupations: Occupation[];
  totalItems: number;
  nameSearchedForm: FormControl;
  isLoading: boolean;

  constructor(
    private contributorsService: ContributorsService,
    private materialDialog: MatDialog,
    private profilesServices: ProfilesService,
    private occupationsService: OccupationsService
  ) { }

  ngOnInit() {
    this.displayedColumns = ['name', 'email', 'profile.name', 'actions'];
    this.nameSearchedForm = new FormControl();
    this.nameSearchedForm.valueChanges.pipe(debounceTime(500)).subscribe(
      (newValue) => {
        this.getContributors({ search: newValue });
      }
    );
    this.getContributors();
  }

  getContributors(params?: { search?: string, page?: number }): void {
    this.isLoading = true;
    this.contributorsService.getContributors(params)
      .pipe(finalize(() => {
        this.isLoading = false;
      }))
      .subscribe(
        (response: { items: Contributor[], totalItems: number }) => {
          this.contributors = response.items;
          this.totalItems = response.totalItems;
        }
      );
  }

  changedPage(page) {
    this.getContributors({ search: this.nameSearchedForm.value, page: page.pageIndex });
  }

  loadModalData(): Observable<void> {
    return Observable.create(
      (observer) => {
        forkJoin([this.profilesServices.getProfiles({}), this.occupationsService.getOccupations({})]).subscribe(
          (response) => {
            this.profiles = response[0].items;
            this.occupations = response[1].items;
            observer.next();
            observer.complete();
          }
        );
      }
    );
  }

  showContributorModal(contributor?: Contributor) {
    if (!this.profiles || !this.occupations) {
      this.loadModalData().subscribe(
        () => {
          this.showModal(contributor);
        }
      );
    } else {
      this.showModal(contributor);
    }
  }

  showModal(contributor?: Contributor) {
    const dialogRef = this.materialDialog.open(ContributorFormModalComponent,
      {
        maxWidth: '400px',
        width: '400px',
        maxHeight: '90vh',
        data: {
          profiles: this.profiles,
          occupations: this.occupations,
          contributor: contributor ? Object.assign({}, contributor) : undefined
        }
      });
    dialogRef.afterClosed().subscribe(
      (response) => {
        if (response && response.success) {
          this.getContributors();
        }
      }
    );
  }

  deleteContributor(contributor: Contributor) {
    const dialogRef = this.materialDialog.open(ContributorDeleteModalComponent, {
      data: {
        contributor: contributor
      }
    });
    dialogRef.afterClosed().subscribe(
      (response: { removed: boolean }) => {
        if (response && response.removed) {
          this.getContributors();
        }
      }
    );
  }

}
