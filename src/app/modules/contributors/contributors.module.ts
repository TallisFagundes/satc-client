import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgSelectModule } from '@ng-select/ng-select';
import {
  MatTableModule,
  MatInputModule,
  MatIconModule,
  MatButtonModule,
  MatPaginatorModule,
  MatPaginatorIntl,
  MatDialogModule,
  MatMenuModule,
  MatProgressSpinnerModule,
  MatProgressBarModule
} from '@angular/material';

import { ContributorsRoutingModule, routedComponents } from './contributors-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatPaginatorIntlCro } from 'src/app/models/paginator';
import { ContributorFormModalComponent } from './contributor-form-modal/contributor-form-modal.component';
import { NgxMaskModule } from 'ngx-mask';
import { ContributorDeleteModalComponent } from './contributor-delete-modal/contributor-delete-modal.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatButtonModule,
    ContributorsRoutingModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatMenuModule,
    MatProgressBarModule,
    NgSelectModule,
    NgxMaskModule
  ],
  entryComponents: [ContributorFormModalComponent, ContributorDeleteModalComponent],
  providers: [{ provide: MatPaginatorIntl, useClass: MatPaginatorIntlCro }],
  declarations: [routedComponents]
})
export class ContributorsModule { }
