import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContributorFormModalComponent } from './contributor-form-modal.component';

describe('ContributorFormModalComponent', () => {
  let component: ContributorFormModalComponent;
  let fixture: ComponentFixture<ContributorFormModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContributorFormModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContributorFormModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
