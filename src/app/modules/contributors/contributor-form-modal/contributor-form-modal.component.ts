import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Contributor } from 'src/app/models/contributor';
import { NgForm, FormGroup } from '@angular/forms';
import { Occupation } from 'src/app/models/occupation';
import { Profile } from 'src/app/models/profile';
import { ContributorsService } from 'src/app/services/contributors.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-contributor-form-modal',
  templateUrl: './contributor-form-modal.component.html',
  styleUrls: ['./contributor-form-modal.component.scss']
})
export class ContributorFormModalComponent implements OnInit {
  modalTitle: string;
  isLoading: boolean;

  constructor(
    private dialogRef: MatDialogRef<ContributorFormModalComponent>,
    @Inject(MAT_DIALOG_DATA) public modalData: ModalData,
    private contributorsService: ContributorsService
  ) { }

  ngOnInit() {
    if (this.modalData && this.modalData.contributor && this.modalData.contributor.id) {
      this.modalTitle = 'Editar Colaborador';
    } else {
      this.modalTitle = 'Novo Colaborador';
      this.modalData.contributor = new Contributor();
    }
  }

  save(form: NgForm) {
    if (form.valid) {
      this.isLoading = true;
      form.form.disable();
      form.value.id = this.modalData.contributor.id;
      if (form.value.id) {
        this.contributorsService.updateContributor(form.value)
          .pipe(finalize(() => {
            form.form.enable();
            this.isLoading = false;
          }))
          .subscribe(
            () => {
              this.isLoading = false;
              this.dialogRef.close({ success: true });
            }
          );
      } else {
        this.contributorsService.createContributor(form.value)
          .pipe(finalize(() => {
            form.form.enable();
            this.isLoading = false;
          }))
          .subscribe(
            () => {
              this.isLoading = false;
              this.dialogRef.close({ success: true });
            }
          );
      }
    }
  }

  cancel() {
    this.dialogRef.close();
  }

}

export class ModalData {
  contributor: Contributor;
  occupations: Occupation[];
  profiles: Profile[];
}
