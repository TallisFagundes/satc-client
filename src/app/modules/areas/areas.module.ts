import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgSelectModule } from '@ng-select/ng-select';
import {
  MatTableModule,
  MatInputModule,
  MatIconModule,
  MatButtonModule,
  MatPaginatorModule,
  MatPaginatorIntl,
  MatDialogModule,
  MatMenuModule,
  MatProgressSpinnerModule,
  MatProgressBarModule
} from '@angular/material';

import { AreasRoutingModule, routedComponents } from './areas-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatPaginatorIntlCro } from 'src/app/models/paginator';
import { AreaFormModalComponent } from './area-form-modal/area-form-modal.component';
import { NgxMaskModule } from 'ngx-mask';
import { AreaDeleteModalComponent } from './area-delete-modal/area-delete-modal.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatButtonModule,
    AreasRoutingModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatMenuModule,
    MatProgressBarModule,
    NgSelectModule,
    NgxMaskModule
  ],
  entryComponents: [AreaFormModalComponent, AreaDeleteModalComponent],
  providers: [{ provide: MatPaginatorIntl, useClass: MatPaginatorIntlCro }],
  declarations: [routedComponents]
})
export class AreasModule { }
