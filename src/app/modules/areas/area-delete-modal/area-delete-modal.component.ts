import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Area } from 'src/app/models/area';
import { AreasService } from 'src/app/services/areas.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-area-delete-modal',
  templateUrl: './area-delete-modal.component.html',
  styleUrls: ['./area-delete-modal.component.scss']
})
export class AreaDeleteModalComponent implements OnInit {
  isLoading: boolean;

  constructor(
    private dialogRef: MatDialogRef<AreaDeleteModalComponent>,
    @Inject(MAT_DIALOG_DATA) public modalData: ModalData,
    private areasService: AreasService
  ) { }

  ngOnInit() {
  }

  confirm() {
    this.isLoading = true;
    this.areasService.deleteArea(this.modalData.area.id)
      .pipe(finalize(() => this.isLoading = false))
      .subscribe(
        (deleted: boolean) => {
          if (deleted) {
            this.dialogRef.close({ removed: true });
          }
        }
      );
  }

  cancel() {
    this.dialogRef.close({ removed: false });
  }

}

export class ModalData {
  area: Area;
}
