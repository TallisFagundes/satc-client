import { Component, OnInit } from '@angular/core';
import { Area } from '../../../models/area';
import { AreasService } from '../../../services/areas.service';
import { MatDialog } from '@angular/material';
import { AreaFormModalComponent } from '../area-form-modal/area-form-modal.component';
import { ProfilesService } from 'src/app/services/profiles.service';
import { OccupationsService } from 'src/app/services/occupations.service';
import { Observable, forkJoin } from 'rxjs';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { AreaDeleteModalComponent } from '../area-delete-modal/area-delete-modal.component';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-areas',
  templateUrl: './areas.component.html',
  styleUrls: ['./areas.component.scss']
})
export class AreasComponent implements OnInit {
  areas: Area[];
  displayedColumns: string[];
  totalItems: number;
  nameSearchedForm: FormControl;
  isLoading: boolean;

  constructor(
    private areasService: AreasService,
    private materialDialog: MatDialog,
    private profilesServices: ProfilesService,
    private occupationsService: OccupationsService
  ) { }

  ngOnInit() {
    this.displayedColumns = ['name', 'actions'];
    this.nameSearchedForm = new FormControl();
    this.nameSearchedForm.valueChanges.pipe(debounceTime(500)).subscribe(
      (newValue) => {
        this.getAreas({ search: newValue });
      }
    );
    this.getAreas();
  }

  getAreas(params?: { search?: string, page?: number }): void {
    this.isLoading = true;
    this.areasService.getAreas(params)
      .pipe(finalize(() => {
        this.isLoading = false;
      }))
      .subscribe(
        (response: { items: Area[], totalItems: number }) => {
          this.areas = response.items;
          this.totalItems = response.totalItems;
        }
      );
  }

  changedPage(page) {
    this.getAreas({ search: this.nameSearchedForm.value, page: page.pageIndex });
  }

  showAreaModal(area?: Area) {
    const dialogRef = this.materialDialog.open(AreaFormModalComponent,
      {
        maxWidth: '400px',
        width: '400px',
        maxHeight: '90vh',
        data: {
          area: area ? Object.assign({}, area) : undefined
        }
      });
    dialogRef.afterClosed().subscribe(
      (response) => {
        if (response && response.success) {
          this.getAreas();
        }
      }
    );
  }

  deleteArea(area: Area) {
    const dialogRef = this.materialDialog.open(AreaDeleteModalComponent, {
      data: {
        area: area
      }
    });
    dialogRef.afterClosed().subscribe(
      (response: { removed: boolean }) => {
        if (response && response.removed) {
          this.getAreas();
        }
      }
    );
  }

}
