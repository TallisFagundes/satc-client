import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AreasComponent } from './areas-list/areas.component';
import { AreaFormModalComponent } from './area-form-modal/area-form-modal.component';
import { AreaDeleteModalComponent } from './area-delete-modal/area-delete-modal.component';

const routes: Routes = [
  {
    path: '',
    component: AreasComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AreasRoutingModule { }

export const routedComponents = [AreasComponent, AreaFormModalComponent, AreaDeleteModalComponent];
