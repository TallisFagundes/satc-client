import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Area } from 'src/app/models/area';
import { NgForm } from '@angular/forms';
import { Occupation } from 'src/app/models/occupation';
import { Profile } from 'src/app/models/profile';
import { AreasService } from 'src/app/services/areas.service';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-area-form-modal',
  templateUrl: './area-form-modal.component.html',
  styleUrls: ['./area-form-modal.component.scss']
})
export class AreaFormModalComponent implements OnInit {
  modalTitle: string;
  isLoading: boolean;

  constructor(
    private dialogRef: MatDialogRef<AreaFormModalComponent>,
    @Inject(MAT_DIALOG_DATA) public modalData: ModalData,
    private areasService: AreasService
  ) { }

  ngOnInit() {
    if (this.modalData && this.modalData.area && this.modalData.area.id) {
      this.modalTitle = 'Editar Área';
    } else {
      this.modalTitle = 'Novo Área';
      this.modalData.area = new Area();
    }
  }

  save(form: NgForm) {
    if (form.valid) {
      this.isLoading = true;
      form.form.disable();
      form.value.id = this.modalData.area.id;
      if (form.value.id) {
        this.areasService.updateArea(form.value)
          .pipe(finalize(() => {
            form.form.enable();
            this.isLoading = false;
          }))
          .subscribe(
            () => {
              this.isLoading = false;
              this.dialogRef.close({ success: true });
            }
          );
      } else {
        this.areasService.createArea(form.value)
          .pipe(finalize(() => {
            form.form.enable();
            this.isLoading = false;
          }))
          .subscribe(
            () => {
              this.isLoading = false;
              this.dialogRef.close({ success: true });
            }
          );
      }
    }
  }

  cancel() {
    this.dialogRef.close();
  }

}

export class ModalData {
  area: Area;
  occupations: Occupation[];
  profiles: Profile[];
}
