import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AreaFormModalComponent } from './area-form-modal.component';

describe('AreaFormModalComponent', () => {
  let component: AreaFormModalComponent;
  let fixture: ComponentFixture<AreaFormModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AreaFormModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AreaFormModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
