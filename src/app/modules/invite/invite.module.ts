import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InviteRoutingModule, routedComponents } from './invite-routing.module';

@NgModule({
  imports: [
    CommonModule,
    InviteRoutingModule
  ],
  declarations: [routedComponents]
})
export class InviteModule { }
