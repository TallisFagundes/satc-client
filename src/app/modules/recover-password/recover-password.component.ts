import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-recover-password',
  templateUrl: './recover-password.component.html',
  styleUrls: ['./recover-password.component.scss']
})
export class RecoverPasswordComponent implements OnInit {
  emailValue: string = '';
  submittedSuccessfully: boolean = false;

  constructor(private userService: UserService) { }

  ngOnInit() {
  }

  submitted(form: NgForm) {
    if (form.valid) {
      this.userService.requestPassword(form.value).subscribe(
        (response) => {
          console.log(response);
        }
      );
      this.submittedSuccessfully = true;
    }
  }

}
