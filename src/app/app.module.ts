import { BrowserModule } from '@angular/platform-browser';
import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';
import localePtExtra from '@angular/common/locales/extra/pt';
import { NgModule, LOCALE_ID } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { DefaultLayoutModule } from './modules/default-layout/default-layout.module';
import { NgxMaskModule } from 'ngx-mask';
import { MatNativeDateModule } from '@angular/material';
import { ExternalLayoutModule } from './modules/external-layout/external-layout.module';

registerLocaleData(localePt, 'pt', localePtExtra);

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    DefaultLayoutModule,
    ExternalLayoutModule,
    MatNativeDateModule,
    NgxMaskModule.forRoot()
  ],
  declarations: [
    AppComponent
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'pt' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
