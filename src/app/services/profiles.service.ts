import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Profile, ProfileMapper } from '../models/profile';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

export const PROFILES: Profile[] = [
  {
    id: 1,
    name: 'Perfil 1'
  },
  {
    id: 2,
    name: 'Perfil 2'
  },
  {
    id: 3,
    name: 'Perfil 3'
  },
  {
    id: 4,
    name: 'Perfil 4'
  },
  {
    id: 5,
    name: 'Perfil 5'
  }
];

@Injectable({
  providedIn: 'root'
})
export class ProfilesService {

  constructor(private httpClient: HttpClient) { }

  getProfiles(params: { search?: string, page?: number }): Observable<{ items: Profile[], totalItems: number }> {
    let offset = 0;
    if (params && params.page) {
      offset = params.page * 10;
    }
    return this.httpClient.get(`${environment.apiURL}/perfis?limit=${environment.itemsPerPage}&offset=${offset}`)
      .pipe(
        map((response: { itens: any[], total: number }) => {
          const newResponse = {
            items: [],
            totalItems: response.total
          };
          response.itens.forEach((element) => {
            newResponse.items.push(ProfileMapper.toProfile(element));
          });
          return newResponse;
        })
      );
  }

  getProfile(id: number): Observable<any> {
    return this.httpClient.get(`${environment.apiURL}/perfis/${id}`)
      .pipe(
        map((response: any[]) => {
          response.forEach((element) => {
            element.name = element.nome;
            element.nome = undefined;
          });
          return response;
        })
      );
  }

  createProfile(profile: Profile): Observable<Profile> {
    return this.httpClient.post(`${environment.apiURL}/perfis/`, ProfileMapper.fromProfile(profile));
  }

  updateProfile(profile: Profile): Observable<Profile> {
    return this.httpClient.post(`${environment.apiURL}/perfis/update`, ProfileMapper.fromProfile(profile));
  }

  deleteProfile(id: number): Observable<any> {
    return this.httpClient.delete(`${environment.apiURL}/perfis/${id}`);
  }
}
