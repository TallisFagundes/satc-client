import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Cycle, CycleMapper } from '../models/cycle';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

export const CYCLES: Cycle[] = [
  {
    id: 1,
    name: 'Ciclo 1',
    startDate: new Date().toJSON(),
    endDate: new Date().toJSON(),
  },
  {
    id: 2,
    name: 'Ciclo 2',
    startDate: new Date().toJSON(),
    endDate: new Date().toJSON()
  },
  {
    id: 3,
    name: 'Ciclo 3',
    startDate: new Date().toJSON(),
    endDate: new Date().toJSON()
  },
  {
    id: 4,
    name: 'Ciclo 4',
    startDate: new Date().toJSON(),
    endDate: new Date().toJSON()
  },
  {
    id: 5,
    name: 'Ciclo 5',
    startDate: new Date().toJSON(),
    endDate: new Date().toJSON()
  }
];


@Injectable({
  providedIn: 'root'
})
export class CyclesService {

  constructor(private httpClient: HttpClient) { }

  getCycles(params: { search?: string, page?: number }): Observable<{ items: Cycle[], totalItems: number }> {
    let offset = 0;
    if (params && params.page) {
      offset = params.page * 10;
    }
    return this.httpClient.get(`${environment.apiURL}/ciclos?limit=${environment.itemsPerPage}&offset=${offset}`).pipe(
      map((response: { itens: any[], total: number }) => {
        const newResponse = {
          items: [],
          totalItems: response.total
        };
        response.itens.forEach((element) => {
          newResponse.items.push(CycleMapper.toCycle(element));
        });
        return newResponse;
      })
    );
  }

  getCycle(id: number): Observable<Cycle> {
    console.log('TODO Get', id);
    return Observable.create(
      (observer) => {
        setTimeout(() => {
          observer.next(CYCLES[id]);
          observer.complete();
        }, 2000);
      }
    );
  }

  createCycle(cycle: Cycle): Observable<any> {
    return this.httpClient.post(`${environment.apiURL}/ciclos/`, CycleMapper.fromCycle(cycle));
  }

  updateCycle(cycle: Cycle): Observable<any> {
    return this.httpClient.post(`${environment.apiURL}/ciclos/update`, CycleMapper.fromCycle(cycle));

  }

  deleteCycle(id: number): Observable<any> {
    return this.httpClient.delete(`${environment.apiURL}/ciclos/${id}`);
  }
}
