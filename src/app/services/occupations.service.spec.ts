import { TestBed, inject } from '@angular/core/testing';

import { OccupationsService } from './occupations.service';

describe('OccupationsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OccupationsService]
    });
  });

  it('should be created', inject([OccupationsService], (service: OccupationsService) => {
    expect(service).toBeTruthy();
  }));
});
