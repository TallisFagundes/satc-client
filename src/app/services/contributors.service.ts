import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Contributor, ContributorMapper } from '../models/contributor';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

export const CONTRIBUTORS: Contributor[] = [
  {
    id: 1,
    name: 'Sergio Alves',
    cpf: '05991500809',
    rg: '123345',
    email: 'juniorspse@gmail.com',
    password: '123456',
    phone: '79991442464',
    profile: {
      id: 1,
      name: 'Competidor'
    },
    occupations: [
      {
        id: 1,
        name: 'Solucoes de Software para Negocios',
        area: null,
      }
    ]
  },
  {
    id: 2,
    name: 'Tallis Souza',
    cpf: '00000000000',
    rg: '00000000',
    email: 'tallisfagundes@gmail.com',
    password: '123456',
    phone: '79999999999',
    profile: {
      id: 1,
      name: 'Competidor'
    },
    occupations: [
      {
        id: 1,
        name: 'Solucoes de Software para Negocios',
        area: null,
      }
    ]
  },
  {
    id: 3,
    name: 'Sergio Alves',
    cpf: '05991500809',
    rg: '123345',
    email: 'juniorspse@gmail.com',
    password: '123456',
    phone: '79991442464',
    profile: {
      id: 1,
      name: 'Competidor'
    },
    occupations: [
      {
        id: 1,
        name: 'Solucoes de Software para Negocios',
        area: null,
      }
    ]
  },
  {
    id: 4,
    name: 'Tallis Souza',
    cpf: '00000000000',
    rg: '00000000',
    email: 'tallisfagundes@gmail.com',
    password: '123456',
    phone: '79999999999',
    profile: {
      id: 1,
      name: 'Competidor'
    },
    occupations: [
      {
        id: 1,
        name: 'Solucoes de Software para Negocios',
        area: null,
      }
    ]
  },
  {
    id: 5,
    name: 'Sergio Alves',
    cpf: '05991500809',
    rg: '123345',
    email: 'juniorspse@gmail.com',
    password: '123456',
    phone: '79991442464',
    profile: {
      id: 1,
      name: 'Competidor'
    },
    occupations: [
      {
        id: 1,
        name: 'Solucoes de Software para Negocios',
        area: null,
      }
    ]
  },
  {
    id: 6,
    name: 'Tallis Souza',
    cpf: '00000000000',
    rg: '00000000',
    email: 'tallisfagundes@gmail.com',
    password: '123456',
    phone: '79999999999',
    profile: {
      id: 1,
      name: 'Competidor'
    },
    occupations: [
      {
        id: 1,
        name: 'Solucoes de Software para Negocios',
        area: null,
      }
    ]
  }
];

@Injectable({
  providedIn: 'root'
})
export class ContributorsService {

  constructor(private httpClient: HttpClient) { }

  getContributors(params: { search?: string, page?: number }): Observable<{ items: Contributor[], totalItems: number }> {
    let offset = 0;
    if (params && params.page) {
      offset = params.page * 10;
    }
    return this.httpClient.get(`${environment.apiURL}/colaboradores?limit=${environment.itemsPerPage}&offset=${offset}`).pipe(
      map((response: { itens: any[], total: number }) => {
        const newResponse = {
          items: [],
          totalItems: response.total
        };
        response.itens.forEach((element) => {
          newResponse.items.push(ContributorMapper.toContributor(element));
        });
        return newResponse;
      })
    );
  }

  getContributor(id: number): Observable<Contributor> {
    console.log('TODO Get', id);
    return Observable.create(
      (observer) => {
        setTimeout(() => {
          observer.next(CONTRIBUTORS[id]);
          observer.complete();
        }, 2000);
      }
    );
  }

  createContributor(contributor: Contributor): Observable<any> {
    return this.httpClient.post(`${environment.apiURL}/colaboradores/`, ContributorMapper.fromContributor(contributor));
  }

  updateContributor(contributor: Contributor): Observable<Contributor> {
    return this.httpClient.post(`${environment.apiURL}/colaboradores/udpate`, ContributorMapper.fromContributor(contributor));
  }

  deleteContributor(id: number): Observable<boolean> {
    console.log('TODO Delete', id);
    return Observable.create(
      (observer) => {
        setTimeout(() => {
          observer.next(id > 0);
          observer.complete();
        }, 2000);
      }
    );
  }
}
