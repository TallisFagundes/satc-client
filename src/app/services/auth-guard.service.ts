import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private authService: AuthService, private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (state.url.toString().indexOf('login') !== -1) {
      if (this.authService.isLogged()) {
        this.router.navigateByUrl('home');
        return false;
      }
      return true;
    } else {
      if (!this.authService.isLogged()) {
        this.router.navigateByUrl('login');
        return false;
      }
      return true;
    }
  }
}
