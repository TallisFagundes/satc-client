import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Area, AreaMapper } from '../models/area';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

export const AREAS: Area[] = [
  {
    id: 1,
    name: 'Jogos'
  },
  {
    id: 2,
    name: 'Empreendedorismo'
  },
  {
    id: 3,
    name: 'Programação'
  },
  {
    id: 4,
    name: 'Jardinagem'
  },
  {
    id: 5,
    name: 'Design'
  },
  {
    id: 6,
    name: 'Fotografia'
  },
];

@Injectable({
  providedIn: 'root'
})
export class AreasService {

  constructor(private httpClient: HttpClient) { }

  getAreas(params: { search?: string, page?: number }): Observable<{ items: Area[], totalItems: number }> {
    let offset = 0;
    if (params && params.page) {
      offset = params.page * 10;
    }
    return this.httpClient.get(`${environment.apiURL}/areas?limit=${environment.itemsPerPage}&offset=${offset}`).pipe(
      map((response: { itens: any[], total: number }) => {
        const newResponse = {
          items: [],
          totalItems: response.total
        };
        response.itens.forEach((element) => {
          newResponse.items.push(AreaMapper.toArea(element));
        });
        return newResponse;
      })
    );
  }

  getArea(id: number): Observable<Area> {
    console.log('TODO Get', id);
    return Observable.create(
      (observer) => {
        setTimeout(() => {
          observer.next(AREAS[id]);
          observer.complete();
        }, 2000);
      }
    );
  }

  createArea(area: Area): Observable<Area> {
    return this.httpClient.post(`${environment.apiURL}/areas/`, AreaMapper.fromArea(area));
  }

  updateArea(area: Area): Observable<Area> {
    return this.httpClient.post(`${environment.apiURL}/areas/update`, AreaMapper.fromArea(area));
  }

  deleteArea(id: number): Observable<any> {
    return this.httpClient.delete(`${environment.apiURL}/areas/${id}`);
  }
}
