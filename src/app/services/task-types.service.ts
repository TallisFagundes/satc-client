import { Injectable } from '@angular/core';
import { TaskType, TaskTypeMapper } from '../models/task-type';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';
import { map } from 'rxjs/operators';

export const TASKTYPES: TaskType[] = [
  {
    id: 1,
    name: 'Tipo 1',
    usesCIS: false
  },
  {
    id: 2,
    name: 'Tipo 2',
    usesCIS: true
  },
  {
    id: 3,
    name: 'Tipo 3',
    usesCIS: false
  },
  {
    id: 4,
    name: 'Tipo 4',
    usesCIS: true
  },
  {
    id: 5,
    name: 'Tipo 5',
    usesCIS: false
  },
  {
    id: 6,
    name: 'Tipo 6',
    usesCIS: true
  }
];

@Injectable({
  providedIn: 'root'
})
export class TaskTypesService {

  constructor(private httpClient: HttpClient) { }

  getTaskTypes(params: { search?: string, page?: number }): Observable<{ items: TaskType[], totalItems: number }> {
    let offset = 0;
    if (params && params.page) {
      offset = params.page * 10;
    }
    return this.httpClient.get(`${environment.apiURL}/tipostarefa?limit=${environment.itemsPerPage}&offset=${offset}`).pipe(
      map((response: { itens: any[], total: number }) => {
        const newResponse = {
          items: [],
          totalItems: response.total
        };
        response.itens.forEach((element) => {
          newResponse.items.push(TaskTypeMapper.toTaskType(element));
        });
        return newResponse;
      })
    );
  }

  getTaskType(id: number): Observable<TaskType> {
    console.log('TODO Get', id);
    return Observable.create(
      (observer) => {
        setTimeout(() => {
          observer.next(TASKTYPES[id]);
          observer.complete();
        }, 2000);
      }
    );
  }

  createTaskType(taskType: TaskType): Observable<any> {
    return this.httpClient.post(`${environment.apiURL}/tipostarefa/`, TaskTypeMapper.fromTaskType(taskType));
  }

  updateTaskType(taskType: TaskType): Observable<any> {
    return this.httpClient.post(`${environment.apiURL}/tipostarefa/update`, TaskTypeMapper.fromTaskType(taskType));
  }

  deleteTaskType(id: number): Observable<any> {
    return this.httpClient.delete(`${environment.apiURL}/tipostarefa/${id}`);
  }
}
