import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Occupation, OccupationMapper } from '../models/occupation';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

export const OCCUPATIONS: Occupation[] = [
  {
    id: 1,
    name: 'Ocupação 1',
    area: {
      id: 1,
      name: 'Área 1'
    }
  },
  {
    id: 2,
    name: 'Ocupação 2',
    area: {
      id: 2,
      name: 'Área 2'
    }
  },
  {
    id: 3,
    name: 'Ocupação 3',
    area: {
      id: 3,
      name: 'Área 3'
    }
  },
  {
    id: 4,
    name: 'Ocupação 4',
    area: {
      id: 4,
      name: 'Área 4'
    }
  },
  {
    id: 5,
    name: 'Ocupação 5',
    area: {
      id: 5,
      name: 'Área 5'
    }
  }
];

@Injectable({
  providedIn: 'root'
})
export class OccupationsService {

  constructor(private httpClient: HttpClient) { }

  getOccupations(params: { search?: string, page?: number }): Observable<{ items: Occupation[], totalItems: number }> {
    let offset = 0;
    if (params && params.page) {
      offset = params.page * 10;
    }
    return this.httpClient.get(`${environment.apiURL}/ocupacoes?limit=${environment.itemsPerPage}&offset=${offset}`).pipe(
      map((response: { itens: any[], total: number }) => {
        const newResponse = {
          items: [],
          totalItems: response.total
        };
        response.itens.forEach((element) => {
          newResponse.items.push(OccupationMapper.toOccupation(element));
        });
        return newResponse;
      })
    );
  }

  getOccupation(id: number): Observable<Occupation> {
    console.log('TODO Get', id);
    return Observable.create(
      (observer) => {
        setTimeout(() => {
          observer.next(OCCUPATIONS[id]);
          observer.complete();
        }, 2000);
      }
    );
  }

  createOccupation(occupation: Occupation): Observable<Occupation> {
    return this.httpClient.post(`${environment.apiURL}/ocupacoes/`, OccupationMapper.fromOccupation(occupation));
  }

  updateOccupation(occupation: Occupation): Observable<Occupation> {
    return this.httpClient.post(`${environment.apiURL}/ocupacoes/update`, OccupationMapper.fromOccupation(occupation));
  }

  deleteOccupation(id: number): Observable<Occupation> {
    return this.httpClient.delete(`${environment.apiURL}/ocupacoes/${id}`);
  }
}
