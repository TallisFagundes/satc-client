import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { LocalUserData } from '../models/local-user-data';
import { HttpClient } from '@angular/common/http';
import * as jwt_decode from "jwt-decode";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient) { }

  setUserLocalData(userData: LocalUserData): void {
    localStorage.setItem(environment.localStorageStrings.userData, JSON.stringify(userData));
  }

  getUserLocalData(): LocalUserData {
    const usertData = localStorage.getItem(environment.localStorageStrings.userData);
    if (usertData && usertData !== null) {
      return <LocalUserData>JSON.parse(usertData);
    }
    return null;
  }

  getUserData(): Observable<LocalUserData> {
    // TODO
    return Observable.create(
      (observer) => {
        const userData = {
          name: 'Tallis Souza',
          email: 'tallisfagundes@gmail.com'
        };
        observer.next(userData);
        observer.complete();
      }
    );
  }

  requestPassword(email) {
    return this.httpClient.post(`${environment.apiURL}/recuperarsenha`, email);
  }

  getPasswordTokenInfo(token: string) {
    try {
      return jwt_decode(token);
    }
    catch (Error) {
      return null;
    }
  }

  setNewPasswordByToken(token: string, passwordData: { password: string, confirmPassword: string }): Observable<{}> {
    console.log(passwordData);
    return Observable.create(
      (observer) => {
        if (token) {
          observer.next(token);
          observer.complete();
        }
      }
    );
  }
}
