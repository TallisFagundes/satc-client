import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { UserService } from './user.service';
import { LocalUserData } from '../models/local-user-data';
import { ContributorMapper } from '../models/contributor';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private httpClient: HttpClient, private userService: UserService) { }

  isLogged(): boolean {
    const token = localStorage.getItem(environment.localStorageStrings.token);
    return !!token && token !== null;
  }

  login(email: string, password: string): Observable<any> {
    return Observable.create(
      (observer) => {
        this.httpClient.post(`${environment.apiURL}/colaboradores/login`, { email, password }).subscribe(
          (contributor: any) => {
            contributor.itens[0] = ContributorMapper.toContributor(contributor.itens[0]);
            localStorage.setItem(environment.localStorageStrings.token, `${email}${password}`);
            this.userService.setUserLocalData(contributor.itens[0]);
            observer.next();
            observer.complete();
          }
        );
      }
    );
  }

  logout(): void {
    localStorage.clear();
  }
}
