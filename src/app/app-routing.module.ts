import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';
import { DefaultLayoutComponent } from './modules/default-layout/default-layout.component';
import { AuthGuardService } from './services/auth-guard.service';
import { ExternalLayoutComponent } from './modules/external-layout/external-layout.component';


const routes: Routes = [
  {
    path: '',
    component: ExternalLayoutComponent,
    children: [
      {
        path: 'login',
        loadChildren: './modules/login/login.module#LoginModule',
        canActivate: [AuthGuardService]
      },
      {
        path: 'recover-password',
        loadChildren: './modules/recover-password/recover-password.module#RecoverPasswordModule'
      },
      {
        path: 'invite',
        loadChildren: './modules/invite/invite.module#InviteModule'
      },
      {
        path: 'reset-password',
        loadChildren: './modules/reset-password/reset-password.module#ResetPasswordModule'
      }
    ]
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    children: [
      {
        path: 'home',
        loadChildren: './modules/home/home.module#HomeModule'
      },
      {
        path: 'profiles',
        loadChildren: './modules/profiles/profiles.module#ProfilesModule'
      },
      {
        path: 'contributors',
        loadChildren: './modules/contributors/contributors.module#ContributorsModule'
      },
      {
        path: 'areas',
        loadChildren: './modules/areas/areas.module#AreasModule'
      },
      {
        path: 'task-types',
        loadChildren: './modules/task-types/task-types.module#TaskTypesModule'
      },
      {
        path: 'occupations',
        loadChildren: './modules/occupations/occupations.module#OccupationsModule'
      },
      {
        path: 'cycles',
        loadChildren: './modules/cycles/cycles.module#CyclesModule'
      }
    ],
    canActivate: [AuthGuardService]
  },
  {
    path: '**',
    redirectTo: 'login'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
