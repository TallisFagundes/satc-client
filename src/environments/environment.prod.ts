export const environment = {
  production: true,
  apiURL: 'http://177.10.123.242:90/api',
  localStorageStrings: {
    token: 'token',
    userData: 'userData'
  },
  itemsPerPage: 10
};
